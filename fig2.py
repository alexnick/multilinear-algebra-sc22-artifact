import glob
from xml.dom.pulldom import default_bufsize
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import argparse
from typing import List, Tuple 


def read_csv_data(folder: str = '.', frmwrk: str = 'dace_cpu') -> pd.DataFrame:
    df = None
    for filename in glob.glob(f"{folder}/{frmwrk}*.csv"):
        data = pd.read_csv(filename)
        df = pd.concat([df, data])
    return df

def filter_nodes(df: pd.DataFrame, allowed: List[int]) -> pd.DataFrame:
    return df[df.nodes.isin(allowed)]


def get_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = df.groupby(['framework', 'benchmark', 'nodes'])['time'].median()
    medians = medians.reset_index()
    return medians


def get_shmem_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = get_medians(df)
    medians.drop(medians[medians.nodes > 1].index, inplace=True)
    medians.drop(['nodes'], axis=1, inplace=True)
    medians.rename(columns={"benchmark": "benchmark", "time": "time_single"}, inplace=True)
    return medians


def get_scaling(df: pd.DataFrame, shmem_medians: pd.DataFrame) -> pd.DataFrame:
    scaling = df.merge(shmem_medians, how='left', left_on=['benchmark', 'framework'], right_on=['benchmark', 'framework'] )
    scaling['efficiency'] = (scaling['time_single'] / scaling['time'])*100
    return scaling


def split_compute_total(df: pd.DataFrame, bench_filter: str = None) -> Tuple[pd.DataFrame]:
    compute, total = df, df
    if bench_filter:
        mask = df.benchmark.str.endswith(bench_filter)
        compute = df[mask]
        compute.replace(to_replace=bench_filter, value='', regex=True, inplace=True)
        compute['framework'] = compute['framework'].astype(str) + ' Compute'
        total = df[~mask]
    return compute, total


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", type=str, nargs="?", default="results")
    args = vars(parser.parse_args())
    folder = args["folder"]

    dace_cpu_df = read_csv_data(folder, 'dace_cpu')
    ctf_cpu_df = read_csv_data(folder, 'ctf_cpu')

    nodes = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
    dace_cpu_df = filter_nodes(dace_cpu_df, nodes)
    ctf_cpu_df = filter_nodes(ctf_cpu_df, nodes)

    dace_cpu_df.replace('dace_cpu', 'DaCe', inplace=True)
    dace_cpu_df.replace('size', 'sizes', inplace=True)
    ctf_cpu_df.replace('ctf_cpu', 'CTF', inplace=True)
    ctf_cpu_df.replace('size', 'sizes', inplace=True)

    dace_cpu_compute_df, dace_cpu_df = split_compute_total(dace_cpu_df, '_compute')
    dace_cpu_df.drop(['datetime', 'sizes'], axis=1, inplace=True)
    dace_cpu_compute_df.drop(['datetime', 'sizes'], axis=1, inplace=True)
    try:
        ctf_cpu_df.drop(['datetime', 'size'], axis=1, inplace=True)
    except KeyError:
        ctf_cpu_df.drop(['datetime', 'sizes'], axis=1, inplace=True)
    dace_cpu_df['time_type'] = 'total'
    ctf_cpu_df['time_type'] = 'total'
    dace_cpu_compute_df['time_type'] = 'comp'
    dace_cpu_compute_df.replace('DaCe Compute', 'DaCe', inplace=True)
    all_cpu_df = pd.concat([dace_cpu_df, dace_cpu_compute_df, ctf_cpu_df], axis=0)


    
    all_cpu_df.replace('1mm', '1MM', inplace=True)
    all_cpu_df.replace('2mm', '2MM', inplace=True)
    all_cpu_df.replace('3mm', '3MM', inplace=True)
    all_cpu_df.replace('mttkrp_order_3_mode_0', 'MTTKRP-O3-M0', inplace=True)
    all_cpu_df.replace('mttkrp_order_3_mode_1', 'MTTKRP-O3-M1', inplace=True)
    all_cpu_df.replace('mttkrp_order_3_mode_2', 'MTTKRP-O3-M2', inplace=True)
    all_cpu_df.replace('mttkrp_order_5_mode_0', 'MTTKRP-O5-M0', inplace=True)
    all_cpu_df.replace('mttkrp_order_5_mode_2', 'MTTKRP-O5-M2', inplace=True)
    all_cpu_df.replace('mttkrp_order_5_mode_4', 'MTTKRP-O5-M4', inplace=True)
    all_cpu_df.replace('ttmc_order_5_mode_0', 'TTMc-O5-M0', inplace=True)
    all_cpu_df.replace('ttmc_order_5_mode_0_nosoap', 'TTMc-O5-M0_nosoap', inplace=True)
    all_cpu_df.replace('ttmc_order_3_mode_0', 'TTMc-O3-M0', inplace=True)
    
    wanted_benchmarks = ['1MM', '2MM', '3MM', 'MTTKRP-O3-M0', 'MTTKRP-O3-M1', 'MTTKRP-O3-M2', 'MTTKRP-O5-M0', 'MTTKRP-O5-M2', 'MTTKRP-O5-M4', 'TTMc-O5-M0']
    all_cpu_df = all_cpu_df[all_cpu_df.benchmark.isin(wanted_benchmarks)]


    
    
    g = sns.FacetGrid(data=all_cpu_df[all_cpu_df.time_type == 'total'], col="benchmark", col_order=wanted_benchmarks, col_wrap=5, sharey=False, sharex=False)
    g.map_dataframe(sns.barplot, x='nodes', y="time", hue="framework", palette=sns.color_palette("husl", 3))
 

    for count, lax in enumerate(g.axes):
        title = lax.get_title()
        title = title.lstrip('benchmark = ')
        lax.set_title(title)
        local_data = all_cpu_df[ all_cpu_df.benchmark.isin([title]) & (all_cpu_df.time_type == "comp") ] 
        fake_ctf = local_data.replace('DaCe', 'CTF')
        fake_ctf['time']=0
        local_data = pd.concat([local_data,fake_ctf], axis=0)
        if len(local_data) == 0:
            print("Missing data for " + str(title))
            continue
        print(local_data)
        sns.barplot(ax=lax, data=local_data, x='nodes', y='time', hue='framework')
        if count < len(g.axes)-5:
            # show xlabel only on last three plots
            lax.set_xlabel("")
        else:
            lax.set_xlabel("#Processes (12c each)")
        if count % 5 != 0:
            # show xlabel only on leftmost plots
            lax.set_ylabel("")
        else:
            lax.set_ylabel("Time [s]")
        if count == 0:
            # rename dace legends to comm/comp
            h, l = lax.get_legend_handles_labels()
            lax.legend(handles=[item for item in h[:-1]], labels=['Deinsum (tot)', 'CTF (tot)', 'Deinsum (comp)'], fontsize=10)
        else:
            lax.get_legend().remove()

      
    g.set_xticklabels(rotation=45)
    g.savefig('fig2.pdf', bbox_inches='tight')


