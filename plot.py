import glob
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import argparse
from typing import List, Tuple 


def read_csv_data(folder: str = '.', frmwrk: str = 'dace_cpu') -> pd.DataFrame:
    df = None
    for filename in glob.glob(f"{folder}/{frmwrk}*.csv"):
        data = pd.read_csv(filename)
        df = pd.concat([df, data])
    return df

def filter_nodes(df: pd.DataFrame, allowed: List[int]) -> pd.DataFrame:
    return df[df.nodes.isin(allowed)]


def get_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = df.groupby(['framework', 'benchmark', 'nodes'])['time'].median()
    medians = medians.reset_index()
    return medians


def get_shmem_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = get_medians(df)
    medians.drop(medians[medians.nodes > 1].index, inplace=True)
    medians.drop(['nodes'], axis=1, inplace=True)
    medians.rename(columns={"benchmark": "benchmark", "time": "time_single"}, inplace=True)
    return medians


def get_scaling(df: pd.DataFrame, shmem_medians: pd.DataFrame) -> pd.DataFrame:
    scaling = df.merge(shmem_medians, how='left', left_on=['benchmark', 'framework'], right_on=['benchmark', 'framework'] )
    scaling['efficiency'] = (scaling['time_single'] / scaling['time'])*100
    return scaling


def split_compute_total(df: pd.DataFrame, bench_filter: str = None) -> Tuple[pd.DataFrame]:
    compute, total = df, df
    if bench_filter:
        mask = df.benchmark.str.endswith(bench_filter)
        compute = df[mask]
        compute.replace(to_replace=bench_filter, value='', regex=True, inplace=True)
        compute['framework'] = compute['framework'].astype(str) + ' Compute'
        total = df[~mask]
    return compute, total


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", type=str, nargs="?", default="results")
    args = vars(parser.parse_args())
    folder = args["folder"]

    dace_cpu_df = read_csv_data(folder, 'dace_cpu')
    ctf_cpu_df = read_csv_data(folder, 'ctf_cpu')

    nodes = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
    dace_cpu_df = filter_nodes(dace_cpu_df, nodes)
    ctf_cpu_df = filter_nodes(ctf_cpu_df, nodes)

    dace_cpu_df.replace('dace_cpu', 'DaCe', inplace=True)
    dace_cpu_df.replace('size', 'sizes', inplace=True)
    ctf_cpu_df.replace('ctf_cpu', 'CTF', inplace=True)
    ctf_cpu_df.replace('size', 'sizes', inplace=True)

    dace_cpu_compute_df, dace_cpu_df = split_compute_total(dace_cpu_df, '_compute')
    # print(dace_cpu_compute_df)
    # print(dace_cpu_df)

    dace_cpu_medians = get_medians(dace_cpu_df)
    dace_cpu_compute_medians = get_medians(dace_cpu_compute_df)

    dace_cpu_shmem_medians = get_shmem_medians(dace_cpu_compute_df)
    dace_cpu_compute_scaling = get_scaling(dace_cpu_compute_df, dace_cpu_shmem_medians)
    print(dace_cpu_compute_scaling)
    dace_cpu_shmem_medians.replace("DaCe Compute", "DaCe", inplace=True)
    dace_cpu_scaling = get_scaling(dace_cpu_df, dace_cpu_shmem_medians)

    ctf_cpu_medians = get_medians(ctf_cpu_df)
    ctf_cpu_shmem_medians = get_shmem_medians(ctf_cpu_df)
    ctf_cpu_scaling = get_scaling(ctf_cpu_df, ctf_cpu_shmem_medians)

    all_medians = pd.concat([dace_cpu_medians, dace_cpu_compute_medians, ctf_cpu_medians])
    # scaling = pd.concat([dace_cpu_scaling, dace_cpu_compute_scaling, ctf_cpu_scaling])
    scaling = pd.concat([dace_cpu_scaling, ctf_cpu_scaling])
    scaling.reset_index(inplace=True)

    # print(all_medians)
    # print(scaling)

    g = sns.relplot(data=scaling, x="nodes", y="efficiency", kind="line", hue="framework", style="framework", col="benchmark", col_wrap=3, facet_kws={'sharey': True, 'sharex': True},
                    dashes=False, markers=['v','v'], height=5, aspect=1.5, legend=False, palette=["#F55B4E", "#76b900"])  # palette=["#F55B4E", "#69A8F5", "#76b900"])
    [plt.setp(ax.get_xticklabels(), rotation=90) for ax in g.axes.flat]
    #ax.legend.set_title("Scaling Efficiency [%]")
    g.set_titles(row_template = '{row_name}', col_template = '{col_name}')
    g.set(xscale="log")
    g.set(
        xlim=(1, 520),
        # xticks=[1, 2, 4, 6, 8, 12, 16, 27, 32, 64, 125, 128, 252, 256, 512],
        # xticklabels=[1, 2, 4, 6, 8, 12, 16, 27, 32, 64, 125, 128, 252, 256, 512],
        xticks=[1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
        xticklabels=[1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
        xlabel="Procs (12c)",
        ylim=(0, 150),
        ylabel="Parallel Efficiency [%]")
    plt.subplots_adjust(wspace=0.5)


    ax2 = None # save the last axis object
    for i, ax in enumerate(g.axes.flat):
        kernel = ax.get_title()
        dace = all_medians[ (all_medians['benchmark']==kernel) & (all_medians['framework']=="DaCe") ]
        dace_compute = all_medians[ (all_medians['benchmark']==kernel) & (all_medians['framework']=="DaCe Compute") ]
        ctf = all_medians[ (all_medians['benchmark']==kernel) & (all_medians['framework']=="CTF") ]
        ax2 = ax.twinx()
        ax2.set_ylim(ymin=0.0, ymax=all_medians[all_medians['benchmark']==kernel]['time'].max() * 1.05)
        if len(dace['nodes']):
            dace_plot = ax2.plot(dace['nodes'], dace['time'], color="#F55B4E", linestyle="dashed", marker="o", markersize=4)
            dace_compute_plot = ax2.plot(dace_compute['nodes'], dace_compute['time'], color="#69A8F5", linestyle="dashed", marker="o", markersize=4)
            ctf_plot = ax2.plot(ctf['nodes'], ctf['time'], color="#76b900", linestyle="dashed", marker="o", markersize=4)
            #plt.text(x=512, y=dace['time'].iloc[-1] +1, s="18k cores", rotation=90)
        if i==2:
            ax2.set_ylabel('Runtime [s]', fontsize=12)
    ax2.set_ylabel('Runtime [s]', fontsize=12)

    from matplotlib.lines import Line2D
    custom_lines_r = [Line2D([0], [0],  marker="o", color="#F55B4E"), Line2D([0], [0], marker="o", color="#69A8F5"), Line2D([0], [0], marker="o", color="#76b900")]
    for r in custom_lines_r:
        r.set_linestyle("--")
    # custom_lines_e = [Line2D([0], [0],  marker="v", color="#F55B4E"), Line2D([0], [0], marker="v", color="#69A8F5"), Line2D([0], [0], marker="v", color="#76b900")]
    custom_lines_e = [Line2D([0], [0],  marker="v", color="#F55B4E"), Line2D([0], [0], marker="v", color="#76b900")]
    ax.legend(custom_lines_r + custom_lines_e,
            ['DaCe Runtime', 'DaCe Compute Runtime', 'CTF Runtime',
            # 'DaCe Parallel Efficiency', 'DaCe Compute Parallel Efficiency', 'CTF Parallel Efficiency'],
            'DaCe Parallel Efficiency', 'CTF Parallel Efficiency'],
            title="", ncol=1, bbox_to_anchor=(1.85, 1.55))


    # compute geomean of scaling efficiency improvement
    tmp = scaling.drop(['sizes', 'time', 'time_single'], axis=1).pivot_table(index=['benchmark', 'nodes'], columns=['framework'], values=['efficiency']).reset_index()
    tmp.columns = ["_".join(v) for v in tmp.columns.values]
    improv_l = tmp['efficiency_DaCe']/tmp['efficiency_CTF']
    b = np.log(improv_l)
    print("GeoMean Scaling Eff. improvement over CTF: ", np.exp(b.mean()))
    print("ArithMean Scaling Eff. improvement over CTF: ", improv_l.mean())
    print("Median Scaling Eff. improvement over CTF: ", improv_l.median())



    plt.show()
    g.savefig('facet.pdf', bbox_inches='tight')
