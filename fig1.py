import glob
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import argparse
from typing import List, Tuple 


sns.set(style='whitegrid', rc={"grid.linewidth": 0.1, "legend.fontsize":8.5})
sns.set_context("paper", font_scale=1.3)
            


def read_csv_data(folder: str = '.', frmwrk: str = 'dace_cpu') -> pd.DataFrame:
    df = None
    for filename in glob.glob(f"{folder}/{frmwrk}*.csv"):
        data = pd.read_csv(filename)
        df = pd.concat([df, data])
    return df

def filter_nodes(df: pd.DataFrame, allowed: List[int]) -> pd.DataFrame:
    return df[df.nodes.isin(allowed)]


def get_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = df.groupby(['framework', 'benchmark', 'nodes'])['time'].median()
    medians = medians.reset_index()
    return medians


def get_shmem_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = get_medians(df)
    medians.drop(medians[medians.nodes > 1].index, inplace=True)
    medians.drop(['nodes'], axis=1, inplace=True)
    medians.rename(columns={"benchmark": "benchmark", "time": "time_single"}, inplace=True)
    return medians


def get_scaling(df: pd.DataFrame, shmem_medians: pd.DataFrame) -> pd.DataFrame:
    scaling = df.merge(shmem_medians, how='left', left_on=['benchmark', 'framework'], right_on=['benchmark', 'framework'] )
    scaling['efficiency'] = (scaling['time_single'] / scaling['time'])*100
    return scaling


def split_compute_total(df: pd.DataFrame, bench_filter: str = None) -> Tuple[pd.DataFrame]:
    compute, total = df.copy(), df.copy()
    if bench_filter:
        mask = df.benchmark.str.endswith(bench_filter)
        compute = df[mask]
        compute.replace(to_replace=bench_filter, value='', regex=True, inplace=True)
        compute['framework'] = compute['framework'].astype(str) + ' Compute'
        total = df[~mask]
    return compute, total


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", type=str, nargs="?", default="results")
    args = vars(parser.parse_args())
    folder = args["folder"]

    dace_cpu_df = read_csv_data(folder, 'dace_cpu')
    ctf_cpu_df = read_csv_data(folder, 'ctf_cpu')

    nodes = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
    dace_cpu_df = filter_nodes(dace_cpu_df, nodes)
    ctf_cpu_df = filter_nodes(ctf_cpu_df, nodes)

    dace_cpu_df.replace('dace_cpu', 'DaCe', inplace=True)
    dace_cpu_df.replace('size', 'sizes', inplace=True)
    ctf_cpu_df.replace('ctf_cpu', 'CTF', inplace=True)
    ctf_cpu_df.replace('size', 'sizes', inplace=True)

    dace_cpu_compute_df, dace_cpu_df = split_compute_total(dace_cpu_df, '_compute')
    # print(dace_cpu_compute_df)
    # print(dace_cpu_df)

    dace_cpu_medians = get_medians(dace_cpu_df)
    ctf_cpu_medians = get_medians(ctf_cpu_df)

    merged_medians = dace_cpu_medians.merge(ctf_cpu_medians, on=["benchmark", "nodes"], suffixes=["_dace","_ctf"])
    merged_medians = merged_medians.drop(['framework_dace', 'framework_ctf'], axis=1)
    merged_medians['speedup'] = merged_medians['time_ctf'] / merged_medians['time_dace']
    
    merged_medians.replace('1mm', "1MM", inplace=True)
    merged_medians.replace('2mm', "2MM", inplace=True)
    merged_medians.replace('3mm', "3MM", inplace=True)
    merged_medians.replace('mttkrp_order_3_mode_0', "MTTKRP-O3-M0", inplace=True)
    merged_medians.replace('mttkrp_order_3_mode_1', "MTTKRP-O3-M1", inplace=True)
    merged_medians.replace('mttkrp_order_3_mode_2', "MTTKRP-O3-M2", inplace=True)
    merged_medians.replace('mttkrp_order_5_mode_0', "MTTKRP-O5-M0", inplace=True)
    merged_medians.replace('mttkrp_order_5_mode_2', "MTTKRP-O5-M2", inplace=True)
    merged_medians.replace('mttkrp_order_5_mode_4', "MTTKRP-O5-M4", inplace=True)
    merged_medians.replace('ttmc_order_5_mode_0', "TTMc-O5-M0", inplace=True)

    markers = {"1MM": "v", "2MM": "o", "3MM": "^",  
               "MTTKRP-O3-M0": "<", "MTTKRP-O3-M1": ">", "MTTKRP-O3-M2": "8", 
               "MTTKRP-O5-M0": "D", "MTTKRP-O5-M2": "P", "MTTKRP-O5-M4": "d",
               "TTMc-O5-M0": "X"}
    g = sns.relplot(data=merged_medians, x="nodes", y="speedup", kind="line", hue="benchmark", style="benchmark", markers=markers, 
                    height=4.3, aspect=16/9, linewidth=1.5)
    g._legend.remove()
    t = g.set(xlabel="#Processes (12c each)")
    t = g.set(ylabel="Speedup of Deinsum over CTF")
    t = g.set(xscale="log")
    t = g.set(yscale="log")
    xticks = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
    yticks = [0.25, 0.5, 1, 2, 4, 8, 16, 32]
    t = g.set(xticks=xticks)
    t = g.set(xticklabels=xticks)
    t = g.set(yticks=yticks)
    t = g.set(yticklabels=yticks)
    g.ax.get_xaxis().set_tick_params(which='minor', size=0)
    g.ax.get_xaxis().set_tick_params(which='minor', width=0) 
    g.ax.get_yaxis().set_tick_params(which='minor', size=0)
    g.ax.get_yaxis().set_tick_params(which='minor', width=0)

    #g.ax.tick_params(axis='x', labelsize=16)
    
    t = plt.axhline(y = 1, color = 'black', linestyle = 'dotted', linewidth=0.3)
    t = plt.legend(loc='lower right', title="", ncol=4, fontsize=10)
    t = plt.show()
    g.savefig('fig1.pdf', bbox_inches='tight')

