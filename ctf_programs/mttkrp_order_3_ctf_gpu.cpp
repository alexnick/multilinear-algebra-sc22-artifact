#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <map>
#include <string>
#include <vector>
#include <ctf.hpp>

using namespace CTF;

void mttkrp_order_3_mode_0(Tensor<double>& X,
                           Matrix<double>& JM,
                           Matrix<double>& KM,
                           Matrix<double>& out,
                           int size,
                           int rank) {

    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    std::vector<double> runtime(10);
    for (auto i = 0; i < 10; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        auto t1 = std::chrono::high_resolution_clock::now();
        out["ia"] = X["ijk"] * JM["ja"] * KM["ka"];
        MPI_Barrier(MPI_COMM_WORLD);
        auto t2 = std::chrono::high_resolution_clock::now();
        runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    }

    std::sort(runtime.begin(), runtime.end());
    double median = (runtime[4] + runtime[5]) / 2.0;
    if (commrank == 0) {
        std::cout << "##### MTTKRP, Order 3, Mode 0 #####" << std::endl;
        std::cout << "Median total runtime: " << median << " seconds." << std::endl << std::flush;
        
        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string filename = "ctf_gpu_" + std::to_string(commsize) + "_nodes.csv"; 
        bool file_exists = std::experimental::filesystem::exists(filename);

        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);

        if (!file_exists){
            file << "datetime,benchmark,framework,nodes,sizes,time" << std::endl;
        }

        for (auto time: runtime) {
            file << std::put_time(std::localtime(&t), "%F %T") << ","
                 << "mttkrp_order_3_mode_0,ctf_gpu,"
                 << commsize << ","
                 << "\"(" << size << "," << size << "," << size << "," << rank << ")\","
                 << time << std::endl;
        }

        file.close();
    }    

}

void mttkrp_order_3_mode_1(Tensor<double>& X,
                           Matrix<double>& IM,
                           Matrix<double>& KM,
                           Matrix<double>& out,
                           int size,
                           int rank) {

    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    std::vector<double> runtime(10);
    for (auto i = 0; i < 10; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        auto t1 = std::chrono::high_resolution_clock::now();
        out["ja"] = X["ijk"] * IM["ia"] * KM["ka"];
        MPI_Barrier(MPI_COMM_WORLD);
        auto t2 = std::chrono::high_resolution_clock::now();
        runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    }

    std::sort(runtime.begin(), runtime.end());
    double median = (runtime[4] + runtime[5]) / 2.0;
    if (commrank == 0) {
        std::cout << "##### MTTKRP, Order 3, Mode 1 #####" << std::endl;
        std::cout << "Median total runtime: " << median << " seconds." << std::endl << std::flush;
        
        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string filename = "ctf_gpu_" + std::to_string(commsize) + "_nodes.csv"; 
        bool file_exists = std::experimental::filesystem::exists(filename);

        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);

        if (!file_exists){
            file << "datetime,benchmark,framework,nodes,sizes,time" << std::endl;
        }

        for (auto time: runtime) {
            file << std::put_time(std::localtime(&t), "%F %T") << ","
                 << "mttkrp_order_3_mode_1,ctf_gpu,"
                 << commsize << ","
                 << "\"(" << size << "," << size << "," << size << "," << rank << ")\","
                 << time << std::endl;
        }

        file.close();
    }    

}

void mttkrp_order_3_mode_2(Tensor<double>& X,
                           Matrix<double>& IM,
                           Matrix<double>& JM,
                           Matrix<double>& out,
                           int size,
                           int rank) {

    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    std::vector<double> runtime(10);
    for (auto i = 0; i < 10; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        auto t1 = std::chrono::high_resolution_clock::now();
        out["ka"] = X["ijk"] * IM["ia"] * JM["ja"];
        MPI_Barrier(MPI_COMM_WORLD);
        auto t2 = std::chrono::high_resolution_clock::now();
        runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    }

    std::sort(runtime.begin(), runtime.end());
    double median = (runtime[4] + runtime[5]) / 2.0;
    if (commrank == 0) {
        std::cout << "##### MTTKRP, Order 3, Mode 2 #####" << std::endl;
        std::cout << "Median total runtime: " << median << " seconds." << std::endl << std::flush;
        
        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string filename = "ctf_gpu_" + std::to_string(commsize) + "_nodes.csv"; 
        bool file_exists = std::experimental::filesystem::exists(filename);

        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);

        if (!file_exists){
            file << "datetime,benchmark,framework,nodes,sizes,time" << std::endl;
        }

        for (auto time: runtime) {
            file << std::put_time(std::localtime(&t), "%F %T") << ","
                 << "mttkrp_order_3_mode_2,ctf_gpu,"
                 << commsize << ","
                 << "\"(" << size << "," << size << "," << size << "," << rank << ")\","
                 << time << std::endl;
        }

        file.close();
    }    

}

int main(int argc, char** argv) {

    MPI_Init(&argc, &argv);
    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    World dw;


    // MTTKRP Order 3
    {
        std::map<int, int> s_scaling {
            {1, 1024}, {2, 1218}, {4, 1450}, {8, 1724}, {12, 1908}, {16, 2048},
            {27, 2337}, {32, 2436}, {64, 2900}, {125, 3425}, {128, 3448},
            {252, 4116}, {256, 4096}, {512, 4872}
        };
        std::map<int, int> r_scaling {
            {1, 24}, {2, 30}, {4, 34}, {8, 42}, {12, 48}, {16, 48},
            {27, 57}, {32, 60}, {64, 68}, {125, 85}, {128, 88},
            {252, 126}, {256, 96}, {512, 120}
        };

        int* lens = new int[3];
        std::fill(lens, lens + 3, s_scaling[commsize]);
        int R = r_scaling[commsize];
        if (commrank == 0) {
            for (auto i = 0; i < 3; ++i) std::cout << lens[i] << " ";
            std::cout << R << std::endl;
        }

        Tensor<double> X(3, false, lens, dw);
        X.fill_random(0.0, 1.0);

        Matrix<double> IM(lens[0], R, dw);
        Matrix<double> JM(lens[1], R, dw);
        Matrix<double> KM(lens[2], R, dw);

        IM.fill_random(0.0, 1.0);
        JM.fill_random(0.0, 1.0);
        KM.fill_random(0.0, 1.0);

        Matrix<double> out0(lens[0], R, dw);
        Matrix<double> out1(lens[1], R, dw);
        Matrix<double> out2(lens[2], R, dw);

        mttkrp_order_3_mode_0(X, JM, KM, out0, s_scaling[commsize], R);
        mttkrp_order_3_mode_1(X, IM, KM, out1, s_scaling[commsize], R);
        mttkrp_order_3_mode_2(X, IM, JM, out2, s_scaling[commsize], R);
    }

    // std::map<int, int> weak_scaling { {1, 60}, {2, 68}, {4, 80}, {8, 92}, {16, 104}, {30, 120}, {32, 120}, {64, 136} };

    // int* lens = new int[5];
    // std::fill(lens, lens+5, weak_scaling[commsize]);
    // if (commrank == 0) {
    //     for (auto i = 0; i < 5; ++i) std::cout << lens[i] << " ";
    //     std::cout << std::endl;
    // }
    // int C = 25;

    // Tensor<double> X(5, false, lens, dw);
    // X.fill_random(0.0, 1.0);

    // Matrix<double> IM(lens[0], C, dw);
    // Matrix<double> JM(lens[1], C, dw);
    // Matrix<double> KM(lens[2], C, dw);
    // Matrix<double> LM(lens[3], C, dw);
    // Matrix<double> MM(lens[4], C, dw);

    // IM.fill_random(0.0, 1.0);
    // JM.fill_random(0.0, 1.0);
    // KM.fill_random(0.0, 1.0);
    // LM.fill_random(0.0, 1.0);
    // MM.fill_random(0.0, 1.0);

    // Matrix<double> outI(lens[0], C, dw);
    // Matrix<double> outM(lens[4], C, dw);
    // Matrix<double> outK(lens[2], C, dw);

    // std::vector<double> runtime(10);
    // for (auto i = 0; i < 10; ++i) {
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     auto t1 = std::chrono::high_resolution_clock::now();
    //     outI["ia"] = X["ijklm"] * JM["ja"] * KM["ka"] * LM["la"] * MM["ma"];
    //     // outM["ma"] = X["ijklm"] * IM["ia"] * JM["ja"] * KM["ka"] * LM["la"];
    //     // outK["ka"] = X["ijklm"] * IM["ia"] * JM["ja"] * LM["la"] * MM["ma"];
    //     // auto tmp = outI.norm2();
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     auto t2 = std::chrono::high_resolution_clock::now();
    //     // if (commrank == 0) {
    //     //     std::cout << "Norm is " << tmp << std::endl;
    //     // }
    //     runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    // }

    // std::sort(runtime.begin(), runtime.end());
    // double median = (runtime[4] + runtime[5]) / 2.0;
    // if (commrank == 0) {
    //     std::cout << "Median runtime is " << median << " seconds." << std::endl;
    // }

    // for (auto i = 0; i < 10; ++i) {
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     auto t1 = std::chrono::high_resolution_clock::now();
    //     // outI["ia"] = X["ijklm"] * JM["ja"] * KM["ka"] * LM["la"] * MM["ma"];
    //     // outM["ma"] = X["ijklm"] * IM["ia"] * JM["ja"] * KM["ka"] * LM["la"];
    //     outK["ka"] = X["ijklm"] * IM["ia"] * JM["ja"] * LM["la"] * MM["ma"];
    //     // auto tmp = outI.norm2();
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     auto t2 = std::chrono::high_resolution_clock::now();
    //     // if (commrank == 0) {
    //     //     std::cout << "Norm is " << tmp << std::endl;
    //     // }
    //     runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    // }

    // std::sort(runtime.begin(), runtime.end());
    // median = (runtime[4] + runtime[5]) / 2.0;
    // if (commrank == 0) {
    //     std::cout << "Median runtime is " << median << " seconds." << std::endl;
    // }

    // for (auto i = 0; i < 10; ++i) {
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     auto t1 = std::chrono::high_resolution_clock::now();
    //     // outI["ia"] = X["ijklm"] * JM["ja"] * KM["ka"] * LM["la"] * MM["ma"];
    //     outM["ma"] = X["ijklm"] * IM["ia"] * JM["ja"] * KM["ka"] * LM["la"];
    //     // outK["ka"] = X["ijklm"] * IM["ia"] * JM["ja"] * LM["la"] * MM["ma"];
    //     // auto tmp = outI.norm2();
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     auto t2 = std::chrono::high_resolution_clock::now();
    //     // if (commrank == 0) {
    //     //     std::cout << "Norm is " << tmp << std::endl;
    //     // }
    //     runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    // }

    // std::sort(runtime.begin(), runtime.end());
    // median = (runtime[4] + runtime[5]) / 2.0;
    // if (commrank == 0) {
    //     std::cout << "Median runtime is " << median << " seconds." << std::endl;
    // }

    MPI_Finalize();
    return 0;
}
