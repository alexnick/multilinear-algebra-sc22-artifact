#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <map>
#include <string>
#include <vector>
#include <ctf.hpp>

using namespace CTF;

void one_mm(Matrix<double>& A, Matrix<double>& B, Matrix<double>& out, int size) {

    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    std::vector<double> runtime(10);
    for (auto i = 0; i < 10; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        auto t1 = std::chrono::high_resolution_clock::now();
        out["ik"] = A["ij"] * B["jk"];
        MPI_Barrier(MPI_COMM_WORLD);
        auto t2 = std::chrono::high_resolution_clock::now();
        runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    }

    std::sort(runtime.begin(), runtime.end());
    double median = (runtime[4] + runtime[5]) / 2.0;
    if (commrank == 0) {
        std::cout << "##### 1MM #####" << std::endl;
        std::cout << "Median total runtime: " << median << " seconds." << std::endl << std::flush;
        
        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string filename = "ctf_cpu_" + std::to_string(commsize) + "_nodes.csv"; 
        bool file_exists = std::experimental::filesystem::exists(filename);

        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);

        if (!file_exists){
            file << "datetime,benchmark,framework,nodes,sizes,time" << std::endl;
        }

        for (auto time: runtime) {
            file << std::put_time(std::localtime(&t), "%F %T") << ","
                 << "1mm,ctf_cpu,"
                 << commsize << ","
                 << "\"(" << size << "," << size << ")\","
                 << time << std::endl;
        }

        file.close();
    }    

}

void two_mm(Matrix<double>& A, Matrix<double>& B, Matrix<double>& C, Matrix<double>& out, int size) {

    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    std::vector<double> runtime(10);
    for (auto i = 0; i < 10; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        auto t1 = std::chrono::high_resolution_clock::now();
        out["il"] = A["ij"] * B["jk"] * C["kl"];
        MPI_Barrier(MPI_COMM_WORLD);
        auto t2 = std::chrono::high_resolution_clock::now();
        runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    }

    std::sort(runtime.begin(), runtime.end());
    double median = (runtime[4] + runtime[5]) / 2.0;
    if (commrank == 0) {
        std::cout << "##### 2MM #####" << std::endl;
        std::cout << "Median total runtime: " << median << " seconds." << std::endl << std::flush;
    
        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string filename = "ctf_cpu_" + std::to_string(commsize) + "_nodes.csv"; 
        bool file_exists = std::experimental::filesystem::exists(filename);

        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);

        if (!file_exists){
            file << "datetime,benchmark,framework,nodes,sizes,time" << std::endl;
        }

        for (auto time: runtime) {
            file << std::put_time(std::localtime(&t), "%F %T") << ","
                 << "2mm,ctf_cpu,"
                 << commsize << ","
                 << "\"(" << size << "," << size << "," << size << ")\","
                 << time << std::endl;
        }

        file.close();
    }    

}

void three_mm(Matrix<double>& A, Matrix<double>& B, Matrix<double>& C, Matrix<double>& D, Matrix<double>& out, int size) {

    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    std::vector<double> runtime(10);
    for (auto i = 0; i < 10; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        auto t1 = std::chrono::high_resolution_clock::now();
        out["im"] = A["ij"] * B["jk"] * C["kl"] * D["lm"];
        MPI_Barrier(MPI_COMM_WORLD);
        auto t2 = std::chrono::high_resolution_clock::now();
        runtime[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1e9;
    }

    std::sort(runtime.begin(), runtime.end());
    double median = (runtime[4] + runtime[5]) / 2.0;
    if (commrank == 0) {
        std::cout << "##### 3MM #####" << std::endl;
        std::cout << "Median total runtime: " << median << " seconds." << std::endl << std::flush;
    
        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string filename = "ctf_cpu_" + std::to_string(commsize) + "_nodes.csv";
        bool file_exists = std::experimental::filesystem::exists(filename);

        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);

        if (!file_exists){
            file << "datetime,benchmark,framework,nodes,sizes,time" << std::endl;
        }

        for (auto time: runtime) {
            file << std::put_time(std::localtime(&t), "%F %T") << ","
                 << "3mm,ctf_cpu,"
                 << commsize << ","
                 << "\"(" << size << "," << size << "," << size << "," << size << ")\","
                 << time << std::endl;
        }

        file.close();
    }    

}

int main(int argc, char** argv) {

    MPI_Init(&argc, &argv);
    int commrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    World dw;

    std::map<int, int> scaling {
        {1, 4096}, {2, 5162}, {4, 6502}, {8, 8192}, {12, 10326}, {16, 10324},
        {27, 13005}, {32, 13004}, {64, 16384}, {125, 20645}, {128, 20648},
        {252, 26040}, {256, 26008}, {512, 32768}
    };

    int size = scaling[commsize];

    if (commrank == 0) {
        std::cout << "Matrix size: " << size << std::endl << std::fflush;
    }

    Matrix<double> A(size, size, dw);
    Matrix<double> B(size, size, dw);
    Matrix<double> C(size, size, dw);
    Matrix<double> D(size, size, dw);
    Matrix<double> out(size, size, dw);

    A.fill_random(0.0, 1.0);
    B.fill_random(0.0, 1.0);
    C.fill_random(0.0, 1.0);
    D.fill_random(0.0, 1.0);
    

    one_mm(A, B, out, size);
    two_mm(A, B, C, out, size);
    three_mm(A, B, C, D, out, size);

    MPI_Finalize();
    return 0;
}