import glob
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import argparse
from typing import List, Tuple 

sns.set(style='whitegrid', rc={"grid.linewidth": 0.1, "legend.fontsize":14})
sns.set_context("paper", font_scale=2.2)


def read_csv_data(folder: str = '.', frmwrk: str = 'dace_cpu') -> pd.DataFrame:
    df = None
    for filename in glob.glob(f"{folder}/{frmwrk}*.csv"):
        data = pd.read_csv(filename)
        df = pd.concat([df, data])
    return df

def filter_nodes(df: pd.DataFrame, allowed: List[int]) -> pd.DataFrame:
    return df[df.nodes.isin(allowed)]


def get_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = df.groupby(['framework', 'benchmark', 'nodes'])['time'].median()
    medians = medians.reset_index()
    return medians


def get_shmem_medians(df: pd.DataFrame) -> pd.DataFrame:
    medians = get_medians(df)
    medians.drop(medians[medians.nodes > 1].index, inplace=True)
    medians.drop(['nodes'], axis=1, inplace=True)
    medians.rename(columns={"benchmark": "benchmark", "time": "time_single"}, inplace=True)
    return medians


def get_scaling(df: pd.DataFrame, shmem_medians: pd.DataFrame) -> pd.DataFrame:
    scaling = df.merge(shmem_medians, how='left', left_on=['benchmark', 'framework'], right_on=['benchmark', 'framework'] )
    scaling['efficiency'] = (scaling['time_single'] / scaling['time'])*100
    return scaling


def split_compute_total(df: pd.DataFrame, bench_filter: str = None) -> Tuple[pd.DataFrame]:
    #compute, total = df, df
    if bench_filter:
        mask = df.benchmark.str.endswith(bench_filter)
        compute = df[mask]
        compute.replace(to_replace=bench_filter, value='', regex=True, inplace=True)
        compute['framework'] = compute['framework'].astype(str) + ' Compute'
        total = df[~mask]
    return compute, total


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", type=str, nargs="?", default="gpu")
    args = vars(parser.parse_args())
    folder = args["folder"]

    ctf_gpu_df = read_csv_data(folder, 'ctf_gpu')
    dace_gpu_df = read_csv_data(folder, 'dace_gpu')
    dace_gpu_compute, dace_gpu_tot = split_compute_total(dace_gpu_df, "_compute")
    all_df = pd.concat([ctf_gpu_df, dace_gpu_tot], ignore_index=True)
    

    # remove unneeded columns
    all_df = all_df.drop(['datetime', 'sizes'], axis=1)

    # remove unwanted datapoints
    all_df = all_df[~all_df.benchmark.str.endswith("nosoap")]
    all_df = all_df[~all_df.benchmark.str.endswith("compute_cupy")]

    all_df.replace('1mm', '1MM', inplace=True)
    all_df.replace('2mm', '2MM', inplace=True)
    all_df.replace('3mm', '3MM', inplace=True)
    all_df.replace('mttkrp_order_3_mode_0', 'MTTKRP_O3_M0', inplace=True)
    all_df.replace('mttkrp_order_3_mode_1', 'MTTKRP_O3_M1', inplace=True)
    all_df.replace('mttkrp_order_3_mode_2', 'MTTKRP_O3_M2', inplace=True)
    all_df.replace('mttkrp_order_5_mode_0', 'MTTKRP_O5_M0', inplace=True)
    all_df.replace('mttkrp_order_5_mode_2', 'MTTKRP_O5_M2', inplace=True)
    all_df.replace('mttkrp_order_5_mode_4', 'MTTKRP_O5_M4', inplace=True)
    all_df.replace('ttmc_order_5_mode_0', 'TTMc_O5_M0', inplace=True)
    all_df.replace('ttmc_order_5_mode_0_nosoap', 'TTMc_O5_M0_nosoap', inplace=True)
    all_df.replace('ttmc_order_3_mode_0', 'TTMc_O3_M0', inplace=True) 

    all_df.replace('ctf_gpu', 'CTF GPU Accel.', inplace=True)
    all_df.replace('dace_gpu', 'DaCe GPU Accel.', inplace=True)
    all_df.replace('dace_gpu_cupy', 'DaCe GPU only', inplace=True) 


    df = all_df[all_df.framework != 'DaCe GPU only']
    g = sns.catplot(data=df, kind="bar", x="nodes", y="time", ci=.95, estimator=np.median, hue="framework", col="benchmark", col_wrap=5, 
                    col_order=['1MM', '2MM', '3MM', 'MTTKRP_O3_M0', 'MTTKRP_O3_M1', 'MTTKRP_O3_M2', 'MTTKRP_O5_M0', 'MTTKRP_O5_M2', 'MTTKRP_O5_M4', 'TTMc_O5_M0'], 
                    hue_order=['DaCe GPU Accel.','CTF GPU Accel.'],
                    sharey=False, facet_kws={'sharey': False, 'sharex': False},  palette=["#e68193", "#58a141", "#5875a4"])  
    #g.set(xscale="log")
    g._legend.remove()
    g.set(ylabel="Time [s]", xlabel="#Processes (12c each)")
    g.set_xticklabels(rotation=45)

    for i, ax in enumerate(g.axes.flat):
        title = ax.get_title()
        title = title.lstrip('benchmark = ')
        ax.set_title(title)
        local_data = all_df[all_df.benchmark.isin([title]) & all_df.framework.isin(['DaCe GPU only', 'CTF GPU Accel.'])]
        sns.barplot(ax=ax, data=local_data, x='nodes', y='time', hue='framework', hue_order=['DaCe GPU only','CTF GPU Accel.'], 
                    ci=.95, estimator=np.median, palette=["#5875a4", "#58a141",])
        if i > 0:
            ax.get_legend().remove()
        else:
            h, l = ax.get_legend_handles_labels()
            ax.legend(handles=h, labels=['Deinsum GPU (tot)', 'CTF GPU (tot)', 'Deinsum (GPU Res.)'])
        if (i != 0) and (i != 5):
            ax.set(ylabel=None)
        else:
            ax.set(ylabel="Time [s]")
        if (i > 4):
            ax.set(xlabel="#Processes (12c each)")
        else:
            ax.set(xlabel=None)

    g.savefig('gpu.pdf', bbox_inches='tight')
